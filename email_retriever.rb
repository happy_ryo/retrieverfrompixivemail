require "selenium-webdriver"
class EmailRetriever
# Firefox用のドライバを使う
  driver = Selenium::WebDriver.for :phantomjs
  urlList = %w()

  valid_address = /[\.\w\d_-]+([@*☆◯◎□＠]|あっとまーく|あっと)[\w\d_-]+\.[\w\d._-]+/
  urlList.each {
      |url|
    driver.navigate.to url
    element = driver.find_element(:xpath, '//*[@id="wrapper"]/div/div[2]/div/div[1]/div[2]/div[1]')
    text = element.text.gsub(/(\r\n|\r|\n|\f)/, ' ')
    matches = text.match(valid_address)
    if matches
      print url.gsub(/(&ref=rn-b-.*-3)/,'')
      print ','
      print driver.find_element(:xpath, '//*[@id="wrapper"]/div/div[2]/div/div[1]/div[1]/div/a[1]/h1').text.gsub(/,/, '')
      print ','
      puts matches[0].gsub(/(\*|☆|◯|◎|□|＠|★|◆|◇|♡|♥|あっとまーく|あっと)/, '@')
    end
  }

  driver.quit

end