class EmailTest
  valid_address = /[\w\d_-]+[@*☆◯◎□＠][\w\d_-]+\.[\w\d._-]+/
  text = 'aaa iwama*aainc.co.jp aaaaa'
  size = text.match(valid_address).size
  matches = text.match(valid_address)
  for idx in 1..size
    p matches[idx - 1].gsub(/(\*|☆|◯|◎|□|＠)/, '@')
  end
end